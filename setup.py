from distutils.command.install import install
from setuptools import find_packages, setup


__version__ = 'Undefined'
for line in open('igrat/__init__.py'):
    if (line.startswith('__version__')):
        exec(line.strip())

setup(
    name='igrat',
    version=__version__,
    author='Tiago Antao',
    author_email='tra@popgen.net',
    description=('A (population) genomics library'),
    keywords = 'genomics genetics population-genetics',
    url = 'http://github.com/tiagoantao/igrat',
    packages=find_packages(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Topic :: Utilities',
        'License :: OSI Approved :: GNU Affero General Public License v3',
    ],
    cmdclass = {
        'install': install
    }
)
