igrat
=====

This has been deprecated. Please check
[pygenomics](http://github.com/tiagoantao/pygenomics)

A near real-time computing platform for NGS population genetics' data analysis

igrat is mostly deprecated in favour of pygenomics

Requirements
============

ipython
matplotlib
numpy
