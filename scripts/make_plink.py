import argparse
import gzip
import os

import vcf

parser = argparse.ArgumentParser(description='Converts VCF to Plink')
parser.add_argument('vcf', help='VCF file')
parser.add_argument('plink', help='Plink prefix')
parser.add_argument('--sex', help='Sex file')
parser.add_argument('--map', help='Chromosome mappings file')

#Sex file is a tab file with
#VCF_individual_id Sex(M/F/U)
#If there is no sex file (or no info on an individual, an unknown sex will
#   be added

#map is a map of VCF chromosome to plink chromosome (tab file)
#It should exist if the VCF chromosome cannot be mapped to plink

args = parser.parse_args()

vcfname = args.vcf
plinkpref = args.plink

sexes = {}
if args.sex is not None:
    f = open(args.sex)
    sexes = {toks[0]: toks[1] for toks in
             [l.rstrip().split('\t') for l in f.readlines()]}
    f.close()

chrom_map = {}
if args.map is not None:
    f = open(args.map)
    chrom_map = {toks[0]: toks[1] for toks in
                 [l.rstrip().split('\t') for l in f.readlines()]}
    f.close()

recs = vcf.Reader(filename=vcfname)
fs = {}
for rec in recs:
    for sample in rec.samples:
        ind = sample.sample
        sex = sexes.get(ind, 'U')
        p_sex = 1 if sex == 'M' else (2 if sex == 'F' else 3)
        w = gzip.open("%s.tmp" % ind, "w")
        fs[ind] = w
        start = "%s %s 0 0 %d -9 " % (ind, ind, p_sex)
        w.write(start)
    break

fi = vcf.Reader(filename=vcfname)


wmap = open("%s.map" % plinkpref, "w")
done_inds = set()
cnt = 0
tot = 0
recs = vcf.Reader(filename=vcfname)
for rec in recs:
    if not rec.is_snp:
        continue
    tot += 1

    pos = rec.POS
    chrom = rec.CHROM
    pchrom = chrom_map[chrom]
    ref = rec.REF
    alt = rec.ALT[0]
    qual = rec.QUAL
    wmap.write("%s\t%s%d-%s\t0\t%d\n" % (pchrom, chrom, pos, ref, pos))
    for sample in rec.samples:
        indiv = sample.sample
        done_inds.add(indiv)
        accept = sample['GT'] is not None  # this might change...
        w = fs[indiv]
        if accept:
            alleles = sample.gt_alleles
            alls = [ref, alt]
            w.write(" %s %s" % (alls[int(alleles[0])],
                                alls[int(alleles[1])]))
        else:
            w.write(" 0 0")
    cnt += 1
    if cnt % 1000 == 0:
        print (rec.CHROM, rec.POS, cnt, tot)

wmap.close()

for w in fs.values():
    w.close()

w = open("%s.ped" % plinkpref, "w")
for ind in done_inds:
    f = gzip.open("%s.tmp" % ind)
    w.write(f.readline() + "\n")

    os.remove(ind + ".tmp")
w.close()

#os.system("plink --missing-genotype 0 --make-bed --noweb " +
#          "--out plink --file plink")
#sys.exit()
#os.remove("plink.ped")
#os.remove("plink.map")
