# -*- coding: utf-8 -*-
"""
.. module:: math
   :synopsis: Math

.. moduleauthor:: Tiago Antao <tiagoantao@gmail.com>

"""

import collections


def quantile_counter(counter, quant):
    """A memory efficient quantile based on a collection counter

    This is efficient for coverages (e.g lots of value repetitions)

    Parameter:
        - counter - A collection counter
        - quantile - Quantile (e.g. 0.5 = median)
    """
    counter = collections.counter(counter)  # copy
    vals = counter.keys()
    vals.sort()  # inplace sort
    while len(vals) > 2:
        if counter[vals[0]] > counter[vals[-1]]:
            counter[vals[0]] -= counter[vals[-1]]
            del vals[-1]
        elif counter[vals[0]] < counter[vals[-1]]:
            counter[vals[-1]] -= counter[vals[0]]
            del vals[0]
        else:
            del vals[0]
            del vals[-1]
    if len(vals) == 1:
        return vals[0]
    if counter[vals[0]] > counter[vals[1]]:
        return vals[0]
    if counter[vals[0]] < counter[vals[1]]:
        return vals[1]
    return (vals[0] + vals[1]) / 2


def median_counter(counter):
    """A memory efficient median based on a collection counter

    This is efficient for coverages (e.g lots of value repetitions)

    Parameter:
        - counter - A collection counter
    """
    counter = collections.counter(counter)  # copy
    vals = counter.keys()
    vals.sort()  # inplace sort
    while len(vals) > 2:
        if counter[vals[0]] > counter[vals[-1]]:
            counter[vals[0]] -= counter[vals[-1]]
            del vals[-1]
        elif counter[vals[0]] < counter[vals[-1]]:
            counter[vals[-1]] -= counter[vals[0]]
            del vals[0]
        else:
            del vals[0]
            del vals[-1]
    if len(vals) == 1:
        return vals[0]
    if counter[vals[0]] > counter[vals[1]]:
        return vals[0]
    if counter[vals[0]] < counter[vals[1]]:
        return vals[1]
    return (vals[0] + vals[1]) / 2
