"""
Copyright 2013 Tiago Antao

This file is part of igrat.

igrat is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

igrat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with igrat.  If not, see <http://www.gnu.org/licenses/>.
"""

from collections import namedtuple


Genome = namedtuple("Genome", "species version chros")
Genome.__doc__ = """A Genome includes the species, the version of the reference genome
and a chromosome list"""

Chromosome = namedtuple("Chromosome", "name size")
Chromosome.__doc__ = "A chromsome has a name and a size"

#Meta-data for several genomes
builtin = {
    "Ag" : Genome("Anopheles gambiae", "3", [
        Chromosome("2L", 49364325), Chromosome("2R", 61545105),
        Chromosome("3L", 41963435), Chromosome("3R", 53200684),
        Chromosome("X",  24393108), Chromosome("Y_unplaced", 237045),
        Chromosome("UNKN", 43232228)])
}
