"""
Copyright 2013 Tiago Antao

This file is part of igrat.

igrat is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

igrat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with igrat.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse

import vcf

from igrat.cunit import FileImportCompute

class GPImportCompute(FileImportCompute):
    def __init__(self, fname, outputs, key):
        FileImportCompute.__init__(self, fname, outputs)
        self.out_db = self.outputs[0]
        self.key = key

    def compute(self, start, end):
        cstart, pstart = start
        cend, pend = end
        #cstart and cend are expected to be the same
        v = vcf.Reader(filename=self.fname)
        recs = v.fetch(cstart, pstart, pend)
        for rec in recs:
            


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("fname", help="Indexed VCF to load")
    parser.add_argument("db", help="DB")
    parser.add_argument("key", help="key")
    parser.parse_args()
    #TODO: include filter













