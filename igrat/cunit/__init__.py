# -*- coding: utf-8 -*-
"""Instant Gratification -- Computing Units

    :copyright: Copyright 2013 by Tiago Antao
    :license: GNU Affero General Public License, see LICENSE for details

    .. moduleauthor:: Tiago Antao <tra@popgen.net>
"""


class Compute:
    """A mapping computing unit.

       A general unit will process something into something ;)

       Defines the function that will process a chunk.
    """

    def __init__(self, fun):
        """Constructor.

        Arguments:
            - fun - Function that will do the processing
        """
        self.fun = fun

    def compute(self, **kwargs):
        raise NotImplementedError("Subclasses should implement this!")

    def submit_job(self, constructor_params, start, end, **kwargs):
        pass

    def wait(self):
        pass

    #A checking function is needed


class MappingCompute(Compute):
    """A mapping computing unit.

       A mapping unit will process part of the dataset (e.g genome) using a
       certain functionality.
       It has a set of inputs from the database and produces a set of outputs.
    """

    def __init__(self, fun, inputs, outputs):
        """Constructor.

        Args:
           - inputs (list of db) - list of db inputs
           - outputs (list of db) - list of db outputs
        """
        Compute.__init__(self, fun)
        self.inputs = inputs
        self.outputs = outputs


class FileImportCompute(Compute):
    """A Compute unit to import a file.

       A mapping unit will process part of the dataset (e.g genome) using a
       certain functionality.
       It has file as input and produces a set of output database outputs.
    """

    def __init__(self, fun, fname, outputs):
        """Constructor.

        Args:
           fname (str) - file to import
           outputs (list of db) - list of db outputs
        """
        Compute.__init__(self, fun)
        self.fname = fname
        self.outputs = outputs


class FileCompute(Compute):
    """A Compute unit to convert a file into another file.

       A mapping unit will process part of the dataset (e.g genome) using a
       certain functionality.
       It a file has input and another as output
    """

    def __init__(self, fun, fname, wname):
        """Constructor.

        Arguments:
           - fname - file to read
           - wname - file to write
        """
        Compute.__init__(self, fun)
        self.fname = fname
        self.wname = wname


class fileCompute(object):
    """Decorator for FileCompute"""

    def __init__(self, consolidate_fun):
        self.consolidate_fun = consolidate_fun

    def __call__(self, f):
        def wrap(*args, **kwargs):
            fname = args[0]
            wname = args[1]
            computer = file_compute(f, fname, wname)
            computer.compute(**kwargs)
            self.consolidate_fun(computer)
        return wrap

file_compute = None
