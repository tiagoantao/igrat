# -*- coding: utf-8 -*-
"""Instant Gratification

    :copyright: Copyright 2013 by Tiago Antao
    :license: GNU Affero General Public License, see LICENSE for details

    .. moduleauthor:: Tiago Antao <tra@popgen.net>
"""

__version__ = '0.0.1'

try:
    import configparser
except ImportError:
    import ConfigParser as configparser
import os

config = {}


def load_config(fname):
    cf = configparser.ConfigParser()
    cf.read(fname)

#load_config(os.path.expanduser("~/.config/igrat/init.conf"))
