# -*- coding: utf-8 -*-
"""
.. module:: vcf
   :synopsis: Dealing with VCF files

.. moduleauthor:: Tiago Antao <tra@popgen.net>

"""

import vcf

class Filter:
    """A VCF filter, constraining both a the variant and sample level

    Constructor
    - req_var    - list of required VARIANT annotations
    - req_sample - list of required SAMPLE annotations
    """

    def __init__(self, req_var, req_sample):
        self.req_var = req_var
        self.req_sample = req_sample

    def check_reqs(self, fname):
        """Checks if a VCF has the required annotations for a filter"""
        f = vcf.Reader(fname)
        formats = f.formats.keys()
        infos = f.infos.keys()
        f.close()
        for req in self.req_var:
            if req not in infos:
                return False
        for req in self.req_sample:
            if req not in formats:
                return False
        return True

    def has_annotations(self, rec):
        """Checks if a variant has all annotations required"""
        for annot in self.req_var:
            if annot not in rec.INFO.keys():
                return False
        return True

    def approve_variant(self, rec):
        """Approves a variant according to a filter""" 
        raise NotImplementedError("Not implemented")

    def approve_sample(self, smp):
        """Approves a sample according to a filter""" 
        raise NotImplementedError("Not implemented")


class GATKFilter(Filter):
    """A VCF filter based on GATK best practices"""

    def __init__(self):
        Filter.__init__(self, ["FS", "HaplotypeScore", "QD", "MQ",
                               "MQRankSum", "ReadPosRankSum"],
                        ["GQ"])

    def approve_variant(self, rec):
        if not rec.is_snp:
            return False
        if len(rec.ALT) != 1:
            # Not biallelic
            return False
        if not self.has_annotations(rec):
            return False
        if rec.INFO["MQ"] < 40:
            return False
        if rec.INFO["QD"] < 2:
            return False
        if rec.INFO["FS"] > 60:
            return False
        if rec.INFO["HaplotypeScore"] > 13:
            return False
        if rec.INFO["MQRankSum"] < -12.5:
            return False
        if rec.INFO["ReadPosRankSum"] < -8:
            return False
        return True

    def approve_sample(self, smp):
        if smp["GQ"] < 30:
            return False
        return True
