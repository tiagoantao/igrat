# -*- coding: utf-8 -*-
"""Instant Gratification -- Genetics -- Annotations

    :copyright: Copyright 2014 by Tiago Antao
    :license: GNU Affero General Public License, see LICENSE for details

.. moduleauthor:: Tiago Antao <tra@popgen.net>

"""
from collections import defaultdict


def load_features(fname):
    feat_type = defaultdict(list)
    feat_name = {}
    for l in open(fname):
        toks = l.rstrip().split('\t')
        name = toks[0]
        type_ = toks[1]
        chrom = toks[2]
        start = int(toks[3])
        end = int(toks[4]) if len(toks) > 4 else None
        feat_name[name] = type_, chrom, start, end
        feat_type[type_].append((name, chrom, start, end))
    return feat_type, feat_name
