# -*- coding: utf-8 -*-
"""
.. module:: cunit
   :synopsis: Genome computing units

.. moduleauthor:: Tiago Antao <tra@popgen.net>

"""

from igrat import cunit

import ig_genetics


class GenomeFileCompute(cunit.FileCompute):
    """A Genome computing unit converting a file to another file."""

    def __init__(self, fun, fname, wname, genome=None, size=None):
        """Constructor.

        Arguments:
            - fun    - Processing function
            - fname  - In name
            - wname  - Out name
            - genome - The genome
            - size   - Chunk size
        """
        cunit.FileCompute(self, fun, fname, wname)
        if genome is None:
            self.genome = ig_genetics.genome
        else:
            self.genome = genome
        if size is None:
            self.chunk = chunk
        else:
            self.chunk = size
        self.class_name = "ig_genetics.cunit.GenomeFileCompute"

    def compute(self, **kwargs):
        for chro, size in self.genome.chromosomes.items():
            for i in range(size // self.chunk + 1):
                start = chro, i * self.chunk + 1
                end = chro, min([(i + 1) * self.chunk, size])
                self.submit_job({"fname": self.fname, "wname": self.wname},
                                start, end, kwargs)


chunk = 100000  # The size of each mapped unit, can/should be changed by user
