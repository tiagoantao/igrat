# -*- coding: utf-8 -*-
"""
.. module:: sam
   :synopsis: Dealing with SAM/BAM files

.. moduleauthor:: Tiago Antao <tra@popgen.net>

"""

import collections

import pysam
import pysamstats

import igrat
import igrat.math


def get_median_coverage(samfile):
    """Returns median the coverage (per chromosome)

    This is based on all the reads (e.g. on a paired read, ignores the
    state of the mate)

    Parameter:
        - samfile - sam(bam) file name

    Return:
        A dictionary with median coverage
    """
    bf = pysam.Samfile(samfile)
    cnt = {}
    for rec in pysamstats.stat_coverage(bf):
        if rec["chrom"] not in cnt:
            cnt[rec["chrom"]] = collections.Counter()
        cnt[rec["chrom"]].update([rec["reads_all"]])
    covs = {}
    for chro, counter in cnt.items():
        covs[chro] = igrat.math.median_counter(counter)
    return covs
