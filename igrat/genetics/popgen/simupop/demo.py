# -*- coding: utf-8 -*-
"""
.. module:: Demography
   :synopsis: Demography creation and management

.. moduleauthor:: Tiago Antao <tra@popgen.net>

"""

import simuPOP as sp


def create_pops(popSizes, loci, loci_pos=[], chrom_types=None,
                      info_fields=[]):
    """A single population."""
    pre_ops = [sp.InitSex()]
    post_ops = []
    if chrom_types is None:
        chrom_types = [sp.AUTOSOME] * len(loci)
    pop = sp.Population(size=popSizes, ploidy=2, loci=loci, lociPos=loci_pos,
                        infoFields=info_fields, chromTypes=chrom_types)
    return pop, pre_ops, post_ops
