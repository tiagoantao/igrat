# -*- coding: utf-8 -*-
"""Instant Gratification -- Genetics -- Population Genetics - simuPOP utils

    :copyright: Copyright 2014 by Tiago Antao
    :license: GNU Affero General Public License, see LICENSE for details

.. moduleauthor:: Tiago Antao <tra@popgen.net>

"""
