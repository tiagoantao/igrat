# -*- coding: utf-8 -*-
"""
.. module:: genome
   :synopsis: Genome creation and management

.. moduleauthor:: Tiago Antao <tra@popgen.net>

"""

import numpy

import simuPOP as sp


def create_unlinked_msats(start_pos, num_ms, max_alleles):
    """Creates a set of unlinked microsatellites.

       Initial distribution dirichelet
    """
    init_ops = []
    pre_ops = []
    for msat in range(num_ms):
        diri = numpy.random.mtrand.dirichlet([1.0] * max_alleles)
        if type(diri[0]) == float:
            diriList = diri
        else:
            diriList = list(diri)
        init_ops.append(sp.InitGenotype(freq=diriList, loci=msat))
        #  This assumes no new alleles are possible (e.g. via mutation)
    return [1] * num_ms, init_ops, pre_ops


def create_linked_pedigree(num_chros, num_snps):
    init_ops = []
    pre_ops = []
    mating_ops = []

    def init_pedigree(pop):
        i = 1
        for ind in pop.individuals():
            i += 1
            geno = ind.genotype()
            nloci = pop.totNumLoci()
            for li in range(nloci):
                geno[li] = 2 * i
                geno[li + nloci] = 2 * i + 1
        return True
    init_ops.append(sp.PyOperator(func=init_pedigree))
    mating_ops.append(sp.Recombinator(rates=1 / num_snps))
    return [num_snps] * num_chros, init_ops, pre_ops, mating_ops
