# -*- coding: utf-8 -*-
'''
    igrat.genetics.popgen.plink
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Parsing of plink output files, plus some utilities

    :copyright: Copyright 2014 by Tiago Antao.
    :license: GNU Affero, see LICENSE for details.
'''
import os


def parseSepHeader(l, sep=' '):
    return [x for x in l.rstrip().split(sep) if x != '']


def parseSepBody(l, sep=' ', fieldIds=None, myTypes=None):
    l = l.rstrip()
    toks = [y for y in [x for x in l.rstrip().split(sep) if x != '']
            if y != '']
    if myTypes:
        for i in range(len(myTypes)):
            if myTypes[i] in [int, float] and toks[i] in ["NA"]:
                toks[i] = None
            else:
                toks[i] = myTypes[i](toks[i])
    if fieldIds:
        rec = {}
        for i in range(len(fieldIds)):
            try:
                rec[fieldIds[i]] = toks[i]
            except IndexError:
                rec[fieldIds[i]] = None
        return rec
    else:
        return toks


def parseSep(f, sep=' ', myTypes=None):
    fieldIds = parseSepHeader(f.readline(), sep)
    for l in f:
        yield parseSepBody(l, sep, fieldIds, myTypes)


def parseLocusMiss(f):
    '''Parse plink.lmiss file'''
    for res in parseSep(f, myTypes=[int, str, int, int, float]):
        yield res


def parseHWE(f):
    '''Parse plink.hwe file'''
    for res in parseSep(f, myTypes=[int, str, str, str, str,
                                    str, float, float, float]):
        geno = res['GENO']
        res['GENO'] = [int(x) for x in geno.split('/')]
        yield res


def parseSexCheck(f):
    for res in parseSep(f, myTypes=[str, str, int, int, str, float]):
        yield res


def parseHet(f):
    for res in parseSep(f, myTypes=[str, str, int, float, int, float]):
        yield res


def parseGenome(f):
    for res in parseSep(f, myTypes=[str, str, str, str, str, str, float, float, float, float, int, float, float, float]):
        yield res


def parseFreq(f):
    for res in parseSep(f, myTypes=[int, str, str, str, float, int]):
        yield res


def parseR2(f):
    for res in parseSep(f, myTypes=[int, int, str, int, int, str, float]):
        yield res


def parseNearest(f):
    f.readline()  # Header file
    for l in f:
        toks = filter(lambda x: x != "", l.rstrip().split(" "))
        t1 = max([12, len(toks[0]) + 1])
        t2 = max([13, len(toks[1]) + 1])
        t3 = max([13, len(toks[5]) + 1])
        t4 = max([13, len(toks[6]) + 1])
        fid = l[:t1].strip()
        iid = l[t1:t1 + t2].strip()
        nn = int(l[t1 + t2:t1 + t2 + 7])
        min_dst = float(l[t1 + t2 + 7:t1 + t2 + 20])
        z = float(l[t1 + t2 + 20:t1 + t2 + 33])
        fid2 = l[t1 + t2 + 33:t1 + t2 + 33 + t3].strip()
        iid2 = l[t1 + t2 + 33 + t3:t1 + t2 + t3 + t4 + 33].strip()
        prop_diff = float(l[t1 + t2 + t3 + t4 + 33:-1])
        yield {"fid": fid, "iid": iid, "nn": nn, "min_dst": min_dst,
               "z": z, "fid2": fid2, "iid2": iid2, "prop_diff": prop_diff}


def getSNPs(bim, acceptFun):
    f = open(bim)
    for l in f:
        toks = l.rstrip().replace(" ", "\t").split("\t")
        chro = int(toks[0])
        snp = toks[1]
        pos = int(toks[3])
        if acceptFun(chro, pos):
            yield snp
    f.close()


def convert2gp(plinkPref, gpPref, popDict, header="plink2gp"):
    #Populations will be sorted by name
    ws = {}
    pops = list(popDict.keys())
    pops.sort()
    wGP = open(gpPref + ".gp", "w")
    wGP.write(header + "\n")
    indivPops = {}

    wPop = open(gpPref + ".pops", "w")
    for pop in pops:
        ws[pop] = open(str(os.getpid()) + "_" + pop, "w")
        wPop.write("%s\n" % pop)
        for fam, id in popDict[pop]:
            indivPops.setdefault((fam, id), []).append(pop)
    wPop.close()

    f = open(plinkPref + ".map")
    for l in f:
        toks = l.rstrip().replace(" ", "\t").split("\t")
        chro = toks[0]
        rs = toks[1]
        pos = toks[3]
        wGP.write("%s/%s/%s\n" % (chro, rs, pos))
    f.close()

    f = open(plinkPref + ".ped")
    for l in f:
        toks = l.rstrip().replace(" ", "\t").split("\t")
        fam = toks[0]
        id = toks[1]
        myPops = indivPops[(fam, id)]
        for pop in myPops:
            ws[pop].write("%s/%s," % (fam, id))
            alleles = toks[6:]
            for i in range(len(alleles) // 2):
                aStr = ""
                for a in [alleles[2 * i], alleles[2 * i + 1]]:
                    if a == "A":
                        aStr += "01"
                    elif a == "C":
                        aStr += "02"
                    elif a == "T":
                        aStr += "03"
                    elif a == "G":
                        aStr += "04"
                    else:
                        aStr += "00"
                ws[pop].write(" " + aStr)
            ws[pop].write("\n")

    for pop in pops:
        ws[pop].close()
        wGP.write("POP\n")
        f = open(str(os.getpid()) + "_" + pop)
        for l in f:
            wGP.write(l)
        f.close()
        os.remove(str(os.getpid()) + "_" + pop)
    wGP.close()
