# -*- coding: utf-8 -*-
"""
.. module:: pca
   :synopsis: Parses phasing files
.. moduleauthor:: Tiago Antao <tra@popgen.net>

Parses and converts phase files

"""


def parse_beagle_phase(f, ind_retain=None, ind_remove=[],
                       snp_retain=None, snp_remove=[], is_phased=False):
    """  Parses a beagle phased file.

    We assume that both space and tab are separating fields.
    The user can specify individuals and SNPs to retain and remove.
        If retain is None then all individuals are to be retained,
        unless explicitely marked to remove.
    The generator below returns a tuple with:
        - Type
        - SNP id or phenotype
        - List of Alleles per haplotype name


    @param f: File descriptor with the content
    @param ind_retain: List of individuals to retain (None = All)
    @param ind_remove: List of individuals to remove
    @param snp_retain: List of SNPs to retain (None = All)
    @param snp_remove: List of SNPs to remove
    @param is_phased: Is phased (output format, extra header)
    @rtype: list, generator
    @return: List of haplotype names (repetitions can happen)
             generator yielding one SNP per call
    """
    if is_phased:
        f.readline()
    haplo_names = f.readline().rstrip().replace(" ", "\t").split("\t")[2:]
    num_haplos = len(haplo_names)

    def getSNP(f, retain, remove, ind_remove_pos):
        for l in f:
            data = l.rstrip().replace(" ", "\t").split("\t")
            I = data[0]
            id = data[1]
            if retain and id not in retain:
                continue
            if id in remove:
                continue
            alleles = []
            for i in range(len(data) - 2):
                if i not in ind_remove_pos:
                    alleles.append(data[i + 2])
            #print len(alleles), len(data), len(ind_remove_pos)
            yield I, id, alleles

    ind_remove_pos = []
    maintained_haplo_names = []
    for i in range(num_haplos):
        if ind_retain:
            if haplo_names[i] not in ind_retain:
                ind_remove_pos.append(i)
                continue
        if haplo_names[i] in ind_remove:
            ind_remove_pos.append(i)
            continue
        maintained_haplo_names.append(haplo_names[i])
    #print num_haplos, len(ind_remove_pos)
    #XXX TODO: return a proper iterator
    return maintained_haplo_names, lambda: getSNP(f, snp_retain,
                                                  snp_remove, ind_remove_pos)


def project_beagle_phase(w, f, ind_retain=None, ind_remove=[],
                         snp_retain=None, snp_remove=[],
                         want_phased=False, is_phased=False):
    inds, g = parse_beagle_phase(f, ind_retain, ind_remove,
                                 snp_retain, snp_remove, is_phased)
    if want_phased:
        w.write("I\tid")
        for i in range(len(inds)):
            w.write("\tcol.%d" % (i + 3))
        w.write("\n")
    w.write("\t".join(["I", "id"] + inds) + "\n")
    for i, id, inds in g():
        w.write("\t".join([i, id] + inds) + "\n")
    w.close()


def parse_impute2(samp_desc, hap_desc, conv_num=True):
    """ Parses a IMPUTE2 file set.

        NOTE: We use the format reported on SHAPEIT. This actually seems
              different from the output from IMPUTE2 itself. Please
              report back if you have problems parsing IMPUTE2 files.

        @param samp_desc: Descriptor for the .samples file
        @param hap_desc: Descriptor for the .haps file
        @param conv_num: Convert allele from 0/1 to allele lettetr
        @rtype: list, generator
                List of sample :famId, indId, missing data
                Iterator with rsid, rsid, pos, (allele1, allele2), data list
                    pair for each sample
    """
    #process sample file
    samps = []
    samp_desc.readline()  # header 1, irrelevant
    samp_desc.readline()  # header 2, irrelevant
    for l in samp_desc:
        toks = l.rstrip().split(" ")
        samps.append((toks[0], toks[1], int(toks[2])))

    def getSNP(hap_desc):
        for l in hap_desc:
            toks = l.rstrip().split(" ")
            alist = []
            alleles = toks[3], toks[4]
            if conv_num:
                for i in range(len(toks[5:])):
                    toks[5 + i] = alleles[int(toks[5 + i])]
            for i in range(len(toks[5:]) // 2):
                alist.append((toks[5 + 2 * i], toks[5 + 2 * i + 1]))
            yield toks[0], toks[1], int(toks[2]), alleles, alist
    #XXX TODO: change retunr
    return samps, lambda: getSNP(hap_desc)


def convert_impute2_to_beagle(samp_desc, hap_desc,
                              beagle_desc, is_phased=True,
                              id_fun=lambda chro, id, pos: id ):
    """  Converts a IMPUTE2 file to BEAGLE format.

        @param samp_desc: Descriptor for the .samples file
        @param hap_desc: Descriptor for the .haps file
        @param beagle_desc: Beagle WRITE descriptor
        @param is_phased: Is phased (output format, extra header)
        @param id_fun: Function to generate the beagle id from the HAPS id
    """
    samples, fgen = parse_impute2(samp_desc, hap_desc)
    w = beagle_desc
    if is_phased:
        w.write("I\tid")
        col = 3
        for i in range(len(samples)):
            w.write("\tcol.%d\tcol.%d" % (col, col + 1))
            col += 2
        w.write("\n")
    w.write("#\tsampleID")
    for samp in samples:
        w.write("\t%s\t%s" % (samp[1], samp[1]))
    w.write("\n")
    for chro, id, pos, alleles, alist in fgen():
        flat = []
        for a1, a2 in alist:
            flat.extend([a1, a2])
        w.write("M\t%s\t%s\n" % (id_fun(chro, id, pos), "\t".join(flat)))

if __name__ == "__main__":
    pass  # basic testing will be here
