# Copyright 2012 by Tiago Antao.  All rights reserved.
# This code is part of the Biopython distribution and governed by its
# license.  Please see the LICENSE file that should have been included
# as part of this package.

"""Phasing of genotype data

The Bio.PopGen.Phasing module provides utiity functionality to help
phasing data.

Application running
===================

Input/Output parsing
====================
"""

# For using with statement in Python 2.5 or Jython
from __future__ import with_statement

__docformat__ = "epytext en"  # not just plaintext


def _test():
    """Run the Bio.PopGen.Phasing module's doctests.

    This will try and locate the unit tests directory, and run the doctests
    from there in order that the relative paths used in the examples work.
    """
    import doctest
    import os
    if os.path.isdir(os.path.join("..", "..", "Tests")):
        print("Runing doctests...")
        cur_dir = os.path.abspath(os.curdir)
        os.chdir(os.path.join("..", "..", "Tests"))
        doctest.testmod()
        os.chdir(cur_dir)
        del cur_dir
        print("Done")
    elif os.path.isdir(os.path.join("Tests", "Fasta")):
        print("Runing doctests...")
        cur_dir = os.path.abspath(os.curdir)
        os.chdir(os.path.join("Tests"))
        doctest.testmod()
        os.chdir(cur_dir)
        del cur_dir
        print("Done")

if __name__ == "__main__":
    _test()
