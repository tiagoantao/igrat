# -*- coding: utf-8 -*-
"""Instant Gratification -- Genetics -- Population Genetics -- Plots

    :copyright: Copyright 2013 by Tiago Antao
    :license: GNU Affero General Public License, see LICENSE for details

    .. moduleauthor:: Tiago Antao <tiagoantao@gmail.com>
"""

import math

import pylab


def _get_defaults(add_ax=True, **kwargs):
    fig = kwargs.get("fig", pylab.figure(figsize=(15, 8)))
    if add_ax:
        ax = kwargs.get("ax", fig.add_subplot(1, 1, 1))
    else:
        ax = None
    return fig, ax


def pca(indivs, weights, comp1=1, comp2=2, **kwargs):
    """Plots a PCA.


    If the cluster is empty, it will plot the name of the individual

    Parameters:
        - indivs     - dict individual -> coordinates
        - weights    - PCA weights (relative or absolute)
        - comp1      - First component to plot (starts at 1)
        - comp2      - Second component to plot
        - cluster    - dict individual -> cluster
        - gray       - List of clusters to "gray out" (requires cluster)
        - tag_indivs - List of individuals to tag (requires cluster)
    """
    comp_cluster = {}
    cluster = kwargs.get("cluster", None)
    tag_indivs = kwargs.get("tag_indivs", [])
    gray = kwargs.get("gray", [])
    if cluster is not None:
        groups = list(set(cluster.values()))
        groups.sort()
        for group in groups:
            comp_cluster[group] = []

    fig, ax = _get_defaults(**kwargs)
    if weights is None:
        ax.set_xlabel('PC %d' % comp1)
        ax.set_ylabel('PC %d' % comp2)
    else:
        ax.set_xlabel('PC %d (%.1f)' % (comp1, 100 * weights[comp1 - 1]))
        ax.set_ylabel('PC %d (%.1f)' % (comp2, 100 * weights[comp2 - 1]))
    xmin, xmax, ymin, ymax = None, None, None, None
    for indiv, indiv_comps in indivs.items():
        x, y = indiv_comps[comp1 - 1], indiv_comps[comp2 - 1]
        if xmin is None:
            xmin = xmax = x
            ymin = ymax = y
        if y < ymin:
            ymin = y
        if y > ymax:
            ymax = y
        if x < xmin:
            xmin = x
        if x > xmax:
            xmax = x
        if cluster is None or indiv in tag_indivs:
            ax.text(x, y, indiv)
        if cluster is not None:
            group = cluster[indiv]
            comp_cluster[group].append((x, y))
    if cluster is None:
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
    else:
        for group in gray:
            if len(comp_cluster[group]) == 0:
                continue
            x, y = zip(*comp_cluster[group])
            ax.plot(x, y, ".", color="#BBBBBB", label=group)
        cnt = 0
        markers = ["o", "+", ","]
        for group in groups:
            if group in gray:
                continue
            if len(comp_cluster[group]) == 0:
                continue
            x, y = zip(*comp_cluster[group])
            ax.plot(x, y, markers[cnt // 7], label=group)
            cnt += 1
        ax.legend(loc="right")
        xmin, xmax = pylab.xlim()
        ax.set_xlim(xmin, xmax + 0.1 * (xmax - xmin))  # space for legend
    return ax


def admix(components, cluster, **kwargs):
    """Plots Admixture.

    Parameters:
        - components  - dict individual -> components
        - cluster     - list of tuple (cluster, [individuals])
    """
    fig, ax = _get_defaults(add_ax=False, **kwargs)
    nrows = kwargs.get("nrows", 1)
    with_names = kwargs.get("with_names", False)
    colors = ["r", "g", "0.25", "b", "y", "0.75", "m", "c", "0.5", "k",
              "#ffebcd", "#0000cd", "#006400", "#daa520", "#b22222",
              "#ff4500", "#a020f0", "#abcdef", "#987654", "#1166ff"]
    all_ind_ks = []
    all_ind_names = []
    for name, inds in cluster:
        all_ind_ks.extend([components[ind] for ind in inds])
        all_ind_names.extend([ind[1] for ind in inds])
    nks = len(all_ind_ks[0])

    def draw_row(sp, row_inds, start_ind, inds_row, ind_names):
        bottoms = [0.0] * len(row_inds)
        for k in range(nks):
            i = 0
            sp.bar(range(len(row_inds)), [ks[k] for ks in row_inds],
                   lw=0, width=1, bottom=bottoms, color=colors[k])
            for ind in row_inds:
                bottoms[i] += ind[k]
                i += 1
        sp.bar(range(len(row_inds)),
               [i % 2 == 0 for i in range(len(row_inds))],
               color="white", alpha=0.4, lw=0, width=1)
        pos = 0
        for name, inds in cluster:
            pos += len(inds)
            if pos > start_ind + inds_row:
                break
            elif pos < start_ind:
                continue
            sp.text(pos - start_ind, 0.5, name, ha="right", va="center",
                    rotation="vertical",
                    fontsize=kwargs.get("popfontsize", "small"),
                    backgroundcolor="white")
            sp.axvline(pos - start_ind, color="black", lw=0.5)
        sp.set_ylim(0, 1.05)
        sp.set_xlim(0, inds_row + 1)
        if with_names:
            pos = 0
            for name in ind_names:
                sp.text(pos, 1.0, name, ha="left", va="top",
                        rotation="vertical",
                        alpha=0.5,
                        fontsize=kwargs.get("indfontsize", "small"),
                        backgroundcolor="white")
                pos += 1

    inds_row = math.ceil(len(all_ind_ks) / nrows)
    for row in range(nrows):
        sp = fig.add_subplot(nrows, 1, row + 1)
        start = row * inds_row
        if row == nrows - 1:
            end = None
        else:
            end = (row + 1) * inds_row  # NOT - 1
        row_inds = all_ind_ks[start:end]
        ind_names = all_ind_names[start:end]
        draw_row(sp, row_inds, start, inds_row, ind_names)

    return fig


def admix_chromosome(genome_plot, components, **kwargs):
    """Plots Admixture.

    Parameters:
        - genome plot - Genome plot for the species
        - components  - List of chrom, position, components

    Components has to be ordered by chromosome and position
    """
    pass
