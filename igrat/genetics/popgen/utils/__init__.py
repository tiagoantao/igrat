# -*- coding: utf-8 -*-

"""
.. module:: pca
  :synopsis: Set of Misc utilities to deal with PopGen data.

.. moduleauthor:: Tiago Antao <tra@popgen.net>
"""


def createTriangular(line, withDiagonal=True):
    """Creates a triangular matrix.

    @type  line: str
    @param line: A string representation, tab or space delimited
    @type  withDiagonal: bool
    @param withDiagonal: Has a diagonal

    Examples:
        line: 0.5 0 1
        withDiagonal: False
        Creates
           [[    X    X    X ],
            [  0.5    X    X ],
            [    0    1    X ]]

        X will be, in practice, None

        line: 0.3 0.5 -2 - 1 2
        withDiagonal: True
        Creates
           [[  0.3   X   X ],
            [  0.5  -2   X ],
            [ None   1   2 ]]
    """
    toks = line.replace("\t", " ").split(" ")
    numLines = 0 if withDiagonal else 1
    rem = 1
    currToks = len(toks)
    if withDiagonal:
        rem += 1
    while currToks > 0:
        numLines += 1
        currToks -= rem
        rem += 1
    matrix = []
    pos = 0
    for row in range(numLines):
        currRow = []
        matrix.append(currRow)
        if withDiagonal:
            breakPos = row + 1
        else:
            breakPos = row
        for col in range(numLines):
            if col >= breakPos:
                currRow.append(None)
            else:
                cnt = toks[pos]
                if cnt != "-":
                    currRow.append(float(toks[pos]))
                else:
                    currRow.append(None)
                pos += 1
    return matrix


def readMatrix(f, hasHeader=True, sep="\t"):
    header = None
    mat = []
    if hasHeader:
        header = f.readline().rstrip().split(sep)
    for l in f:
        toks = l.rstrip().split(sep)
        mat.append(toks)
    return header, mat


if __name__ == "__main__":
    createTriangular("0", False)
    createTriangular("0 1 2", False)
    createTriangular("0 1 2 3 4 5", False)
    createTriangular("0 9")
    createTriangular("0 9 1 2 9")
    createTriangular("0 9 1 2 9 3 4 5 9")
