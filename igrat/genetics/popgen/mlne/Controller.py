# Copyright 2014 by Tiago Antao <tiagoantao@gmail.com>.  All rights reserved.

"""
This module allows to control MLNE.

"""

import os
import sys
import tempfile

from Bio.PopGen.GenePop import read as gp_read


class MLNEController:
    def __init__(self, mlne_dir=None):
        """Initializes the controller.

        The initializer checks for existance and executability of binaries.

        @param mlne_dir is the directory where MLNE is. If empty
                       the system will try to find the binary on the PATH.

        """
        self.platform = sys.platform
        if self.platform == 'win32':
            self.bin_name = 'mne.exe'
        elif self.platform == "darwin":  # Mac missing
            self.bin_name = 'mne'
        else:
            self.bin_name = 'mne'
        if mlne_dir:
            self.mlne_dir = mlne_dir
        else:
            for myDir in os.environ["PATH"].split(os.pathsep):
                if self.bin_name in os.listdir(myDir) and os.access(
                    myDir + os.sep + self.bin_name, os.X_OK
                ) and os.path.isfile(myDir + os.sep + self.bin_name):
                    self.mlne_dir = myDir
                    break
        dir_contents = os.listdir(self.mlne_dir)
        if self.bin_name in dir_contents:
            if not os.access(self.mlne_dir + os.sep +
                             self.bin_name, os.X_OK):
                raise IOError("MLNE not executable")
        else:
            raise IOError("MLNE not available")

    def _get_allele_count(self, gp_rec):
        alleles = [set() for i in range(len(gp_rec.loci_list))]
        pops = gp_rec.populations
        for pop in pops:
            for iname, loci in pop:
                for i, (a1, a2) in enumerate(loci):
                    alleles[i].add(a1)
                    alleles[i].add(a2)
        return [len(alls) for alls in alleles]

    def _recode_alleles(self, gp_rec):
        alleles = [set() for i in range(len(gp_rec.loci_list))]
        pops = gp_rec.populations
        for pop in pops:
            for iname, loci in pop:
                for i, (a1, a2) in enumerate(loci):
                    alleles[i].add(a1)
                    alleles[i].add(a2)
        rec_alleles = [{} for i in range(len(gp_rec.loci_list))]
        for i, alls in enumerate(alleles):
            alls = list(alls)
            alls.sort()
            for i2, a in enumerate(alls):
                rec_alleles[i][a] = i2
        return rec_alleles

    def run_mlne(self, gp_file, with_m, in_equilibrium, max_ne, gens):
        """Executes MLNE.

        """
        with tempfile.TemporaryDirectory() as tempdir:
            rec = gp_read(gp_file)
            cwd = os.getcwd()
            os.chdir(tempdir)
            w = open('MNE_DATA', 'w')
            w.write('%d\n' % (1 if with_m else 0))
            w.write('%d\n' % 1 if in_equilibrium else 0)
            w.write('%d\n' % max_ne)
            w.write('0\n')  # logging
            w.write('0\n')  # num_threads - does not seem to work on Linux
            w.write('%d\n' % len(rec.loci_list))
            w.write('%s\n' % ','.join([str(x)
                                      for x in self._get_allele_count(rec)]))
            w.write('%d\n' % len(rec.populations))
            w.write('%s\n' % ','.join([str(x) for x in gens]))

            all_recode = self._recode_alleles(rec)

            def _get_blank():
                blank = []
                for locus in all_recode:
                    blank.append([0 for i in range(len(locus))])
                return blank
            for pop in rec.populations:
                blank = _get_blank()
                w.write('\n')
                for iname, loci in pop:
                    for i, (a1, a2) in enumerate(loci):
                        blank[i][all_recode[i][a1]] += 1
                        blank[i][all_recode[i][a2]] += 1
                for loci in blank:
                    w.write('%s\n' % ' '.join([str(l) for l in loci]))

            w.close()
            os.system(self.mlne_dir + os.sep + self.bin_name)
            f = open('MNE_OUT')
            l = f.readline()
            while l != '':
                if l.find('PARAMETER') > -1:
                    break
                l = f.readline()
            l = f.readline()
            print(l)
            toks = [tok for tok in l.rstrip().split(' ') if tok != '']

            f.close()
            os.chdir(cwd)
            return float(toks[1]), (float(toks[-1]), float(toks[-2]))
