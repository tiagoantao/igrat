# -*- coding: utf-8 -*-
"""
.. module:: pca
   :synopsis: Dealing with PCA results

.. moduleauthor:: Tiago Antao <tra@popgen.net>

"""


def parse_smart(evec_name, evl_name=None):
    """Parse SMARTPCA evec output.

    Arguments:
        evec_name - the EVEC file
        evl_name  - Optional, full list of weights

    If there is a full list of weights, then the percentage of each
    component will be computed

    Returns:
        weights
        weights_as_percentage
        dict individual -> components
    """
    f = open(evec_name)
    header = f.readline().strip()
    weights = [float(x) for x in header.split(" ")[1:] if x != ""]
    indivs = {}
    for l in f:
        toks = [x for x in l.rstrip().split(" ") if x != ""]
        indivs[toks[0]] = [float(x) for x in toks[1:-1]]
    if evl_name is None:
        return weights, None, indivs
    f = open(evl_name)
    all_weights = [float(x.rstrip().strip()) for x in f.readlines()]
    sum_weights = sum(all_weights)
    return all_weights, [x / sum_weights for x in all_weights], indivs


def cut_individuals(indivs, comp, min=None, max=None):
    """Returns the individuals inside a certain interval for a component

    Parameters:
        - indivs - dictionary indiv -> components
        - comp   - Desired component (1-indexed)
        - min    - Minimum value (can be None)
        - max    - Maximum value (can be None
    """
    new_inds = {}
    for ind, vals in indivs.items():
        if min is not None and vals[comp - 1] < min:
            continue
        if max is not None and vals[comp - 1] > max:
            continue
        new_inds[ind] = vals
    return new_inds
